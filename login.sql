/*
 * SQL - Hospital Management System
 *
 * Users - Admin, Doctor, Nurse
 *
 */

create table accounts(
	user_id serial primary key,
	username varchar(50) unique not null,
	password varchar(50) not null,
	email varchar(255) unique not null,
	created_on timestamp not null,
	last_login timestamp,
	foreign key (user_id) references users (user_id)
);

create table roles(
	role_id serial primary key,
	role_name varchar(255) unique not null
);

create table account_roles(
	user_id int not null,
	role_id int not null,
	grant_date timestamp,
	primary key (user_id, role_id),
	foreign key (role_id) references roles (role_id),
	foreign key (user_id) references accounts (user_id)
);

create table users(
	user_id serial primary key,
	fname varchar(20),
	lname varchar(40),
	gender varchar(20),
	specialization varchar(40)
);

insert into roles (role_name) values ('admin'), ('doctor'), ('staff');

with new_user as (
	insert into users (fname, lname, gender, specialization) values ('Luke', 'Adams', 'Male', 'Cardiology')
	returning user_id as id
),
new_account as (
	insert into accounts (user_id, username, password, email, created_on, last_login) values ((select id from new_user), 'ladams', '1234', 'ladams@gmail.com', now(), now())
	returning user_id as account_id
)
insert into account_roles (user_id, role_id) values ((select account_id from new_account), 2);


with new_user as (
	insert into users (fname, lname, gender, specialization) values ('Adrian', 'Hernandez', 'Male', 'Diagnostic Radiology')
	returning user_id as id
),
new_account as (
	insert into accounts (user_id, username, password, email, created_on, last_login) values ((select id from new_user), 'ahernandez', '12345', 'ahernandez@gmail.com', now(), now())
	returning user_id as account_id
)
insert into account_roles (user_id, role_id, grant_date) values ((select account_id from new_account), 2, now());

with new_user as (
	insert into users (fname, lname, gender, specialization) values ('Colin', 'McAteer', 'Male', 'Admin Support Services')
	returning user_id as id
),
new_account as (
	insert into accounts (user_id, username, password, email, created_on, last_login) values ((select id from new_user), 'cmcateer', '123', 'cmcateer@gmail.com', now(), now())
	returning user_id as account_id
)
insert into account_roles (user_id, role_id, grant_date) values ((select account_id from new_account), 1, now());