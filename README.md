# Cloud Care - Hospital Management

Purpose: the application improves the quality control on the products and services of the hospital.  It allows the ability to optimize and digitize all the processes within the institution.  Improved management visibility helps improve visiuality of hospital, all information, and data regarding the patient, doctor and medicine could be seen by any department easily.  An essential additional feature will help break down the communication barriers between doctors/staff and patients who do not speak English as their primary language.

## Tech

The developments in the application are Spring and Angular.  3D modeling software will be the addition in later process.  PostgreSQL and AWS RDS are implemented for the database.

### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.
